from django.http import JsonResponse


def ping(request):
    data = {"ping": "pong", "coco": "rico"}
    return JsonResponse(data)
